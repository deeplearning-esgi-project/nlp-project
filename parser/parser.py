from document import Document, Interval
from typing import List


class Parser(object):
    def create(self):
        return self

    def read_file(self, filename: str) -> Document:
        with open(filename, 'r', encoding='utf-8') as fp:
            content = fp.read()
        return self.read(content)

    def read(self, text):
        raise NotImplemented


class SimpleTextParser(Parser):
    def read(self, content: str) -> Document:
        return Document.create_from_text(content)

#index list contain column to get => e.g : get second and fourst column => index_list -> [2, 4]
def parse_conllu_line(line, index_list):
    nb_index = len(index_list)

    result = []
    parse_line = line.split('\t')
    for index in index_list:
        result.append(parse_line[index - 1])

    return result



class PosTagParser(Parser):
    def read(self, content: str) -> List[Document]:
        docs = []
        for para in content.split("\n\n"):
            sentences = []
            tokens = []
            labels = []

            for line in para.split('\n'):
                if line[0] != '#':
                    res = parse_conllu_line(line, [2, 4])
                    tokens.append(res[0])
                    labels.append(res[1])

            sentences.append(Interval(0, len(tokens)))
            docs.append(Document.create_from_vectors(tokens,sentences, labels))

        return docs







