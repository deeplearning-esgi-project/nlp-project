from document.document import Document
from conllu.parser import parse, parse_tree
from data import DATA_DIR
from parser import SimpleTextParser, PosTagParser
import os

text = 'L\'Entraînement a fourni un modèle avec un score f1 supérieur à 50% (Évaluation sur données "testing"). '\
+ 'Un service qui accepte un document au format txt et répond avec les résultats.'



#doc = Document.create_from_text(text)

docs = PosTagParser().read_file(os.path.join(DATA_DIR, 'fr-ud-train.conllu'))



d = 1