import unittest
from unittest import TestCase
from document.document import Document
from data import DATA_DIR
from parser import SimpleTextParser, PosTagParser
import os

class TestParser(TestCase):
    def test_simple_parser(self):
        doc = SimpleTextParser().read_file(os.path.join(DATA_DIR, 'data.txt'))
        self.assertEqual(len(doc), 9, 'Failed to detect all token')


    def test_Pos_tag_parser(self):
        docs = PosTagParser().read_file(os.path.join(DATA_DIR, 'fr-ud-train.conllu'))



if __name__ == '__main__':
    unittest.main()