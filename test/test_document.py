import unittest
from unittest import TestCase
from document.document import Document

class TestDoc(TestCase):
    def test_len_doc(self):
        text = 'Je m\'appelle Dimitri. Et la pièce est bleu'
        doc = Document.create_from_text(text)
        self.assertEqual(len(doc), 9, 'Failed to detect all token')

    def test_len_sentences(self):
        text = 'Je m\'appelle Dimitri. Et la pièce est bleu. e'
        doc = Document.create_from_text(text)
        self.assertEqual(len(doc.sentences), 3, 'Failed to detect all sentences')




if __name__ == '__main__':
    unittest.main()