from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from document.sentence import Sentence
from document.token import Token
from document.interval import Interval
import re
from typing import List

def get_shape_category(token):
    if re.match('^[\n]+$', token):  # IS LINE BREAK
        return 'NL'
    if any(char.isdigit() for char in token) and re.match('^[0-9.,]+$', token):  # IS NUMBER (E.G., 2, 2.000)
        return 'NUMBER'
    if re.fullmatch('[^A-Za-z0-9\t\n ]+', token):  # IS SPECIAL CHARS (E.G., $, #, ., *)
        return 'SPECIAL'
    if re.fullmatch('^[A-Z\-.]+$', token):  # IS UPPERCASE (E.G., AGREEMENT, INC.)
        return 'ALL-CAPS'
    if re.fullmatch('^[A-Z][a-z\-.]+$', token):  # FIRST LETTER UPPERCASE (E.G. This, Agreement)
        return '1ST-CAP'
    if re.fullmatch('^[a-z\-.]+$', token):  # IS LOWERCASE (E.G., may, third-party)
        return 'LOWER'
    if not token.isupper() and not token.islower():  # WEIRD CASE (E.G., 3RD, E2, iPhone)
        return 'MISC'
    return 'MISC'


def get_token_start(token):
    return token.start

class Document():

    def __init__(self):
        self.text = None
        self.tokens = []
        self.sentences = []

    def __len__(self):
        return len(self.tokens)

    @staticmethod
    def create_from_text(text: str):

        doc = Document()
        doc.text = text
        words = word_tokenize(text, 'french')

        #Tokenize all text into sentences list
        sentences = sent_tokenize(text.replace('\n', ' '), 'french')

        doc_offset = 0
        for sent in sentences:
            doc.sentences.append(Sentence(doc_offset, doc_offset + len(sent), sent))
            doc_offset += len(sent) + 1

        # Tokenize all sentences with document offset
        doc_offset = 0
        for sentence in sentences:
            for word in word_tokenize(sentence, language='french'):
                doc.tokens.append(Token(doc_offset, doc_offset + len(word), pos_tag='Non-C', shape=get_shape_category(word), text=word))
                doc_offset += len(word) + 1

        return doc

    @staticmethod
    def create_from_vectors(words: List[str], sentences: List[Interval] = None, labels: List[str] = None):
        doc = Document()
        text = []
        offset = 0
        doc.sentences = []
        for sentence in sentences:
            text.append(' '.join(words[sentence.start:sentence.end + 1]) + ' ')
            doc.sentences.append(Interval(offset, offset + len(text[-1])))
            offset += len(text[-1])
        doc.text = ''.join(text)

        offset = 0
        doc.tokens = []
        for word, label in zip(words, labels):
            pos = doc.text.find(word, offset)
            if pos >= 0:
                offset = pos + len(word)
                doc.tokens.append(Token(pos, offset, get_shape_category(word), word, label))
        return doc












