from document.interval import Interval

class Sentence(Interval):

    def __init__(self, start: int, end: int, text: str):
        Interval.__init__(self, start, end)
        self.text = text

    def __repr__(self):
        return 'Sentence({}, {})'.format(self.start, self.end)




