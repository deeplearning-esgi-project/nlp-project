from document.interval import Interval

class Token(Interval):
    # Token representaion of word
    """
    :param document : origin Document class
    :param start start of token in document
    :param end end of token in document
    :param pos_tag nature of token 
    :param shape shape of the token
    :param text strign representation of token
    """
    def __init__(self, start, end, shape, text, pos_tag=''):
        Interval.__init__(self, start, end)
        self.start = start
        self.end = end
        self.pos_tag = pos_tag
        self.shape = shape
        self.text = text

        @property
        def start(self):
            return self.start

        @property
        def end(self):
            return self.end

        @property
        def text(self):
            return self.text

        @property
        def pos_tag(self):
            return self.pos_tag

        @property
        def shape(self):
            return self.shape

        @property
        def text(self):
            return self.text

        def __repr__(self):
            return 'Token({}, {}, {})'.format(self.text, self.start, self.end)
